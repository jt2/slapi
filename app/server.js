var app = require('express')();
var build_ast = require('./parser/toAST.js').build_ast;
var fs = require('fs');
var logger = require('./slapiLogger.js');
var ast = readProgramIntoAst();

var inputs = ast.inputs;

// Create Logs
app.use(function(req, res, next){
	logger.info('received a ' + req.method + ' at ' + req.url);
	next();
});


// app.get('/', function(req, res){
// 	res.send(200, 'hello');
// });

for (var endpoint of ast.endpoints){
	createEndpoint(endpoint);
}

app.listen(3000);


// function getAST(){
// 	return {
// 		inputs: {'x':{
// 			type: 'constant',
// 			value: 5
// 			}},
// 		endpoints: [{
// 			httpVerb: 'get',
// 			path: '/example',
// 			returnValue: 'x'
// 		}]
// 	}
// }

function createEndpoint(endpoint){
	if (endpoint.httpVerb === 'get'){
		app.get(endpoint.path, function(req, res){
			var returnValue = resolveValue(endpoint.returnValue);
			res.set('Content-Type', 'application/json');
			res.send(200, returnValue);
		});
		console.log('created get endpoint at ' + endpoint.path)
	}
 
}

function resolveValue(value){
	// returns value or lazy loads it from expression
	var resolvedValue = inputs[value];
	
	if (resolvedValue.type === 'constant'){
		return resolvedValue.value;
	}
	return resolvedValue.value;
}

function readProgramIntoAst(){
	var filename = process.argv[2];
	var ast;
	var fileContents = fs.readFileSync(filename, 'utf8');
	ast = build_ast(fileContents);

	//console.log(ast);

	return ast;
}