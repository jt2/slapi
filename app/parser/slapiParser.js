// Generated from slapi.g4 by ANTLR 4.5.2
// jshint ignore: start
var antlr4 = require('antlr4/index');
var slapiListener = require('./slapiListener').slapiListener;
var grammarFileName = "slapi.g4";

var serializedATN = ["\u0003\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd",
    "\u0003\r\u001b\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004\t",
    "\u0004\u0003\u0002\u0003\u0002\u0006\u0002\u000b\n\u0002\r\u0002\u000e",
    "\u0002\f\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0002\u0002\u0005\u0002\u0004\u0006\u0002\u0003",
    "\u0003\u0002\n\u000b\u0019\u0002\n\u0003\u0002\u0002\u0002\u0004\u000e",
    "\u0003\u0002\u0002\u0002\u0006\u0013\u0003\u0002\u0002\u0002\b\u000b",
    "\u0005\u0004\u0003\u0002\t\u000b\u0005\u0006\u0004\u0002\n\b\u0003\u0002",
    "\u0002\u0002\n\t\u0003\u0002\u0002\u0002\u000b\f\u0003\u0002\u0002\u0002",
    "\f\n\u0003\u0002\u0002\u0002\f\r\u0003\u0002\u0002\u0002\r\u0003\u0003",
    "\u0002\u0002\u0002\u000e\u000f\u0007\u0003\u0002\u0002\u000f\u0010\u0007",
    "\f\u0002\u0002\u0010\u0011\u0007\u0004\u0002\u0002\u0011\u0012\t\u0002",
    "\u0002\u0002\u0012\u0005\u0003\u0002\u0002\u0002\u0013\u0014\u0007\u0005",
    "\u0002\u0002\u0014\u0015\u0007\b\u0002\u0002\u0015\u0016\u0007\u0006",
    "\u0002\u0002\u0016\u0017\u0007\t\u0002\u0002\u0017\u0018\u0007\u0007",
    "\u0002\u0002\u0018\u0019\u0007\f\u0002\u0002\u0019\u0007\u0003\u0002",
    "\u0002\u0002\u0004\n\f"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ null, "'let'", "'='", "'when'", "'at'", "'return'" ];

var symbolicNames = [ null, null, null, null, null, null, "HTTPVERB", "PATH", 
                      "NUMBER", "JSONOBJECT", "ID", "WS" ];

var ruleNames =  [ "file", "constant", "endpoint" ];

function slapiParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

slapiParser.prototype = Object.create(antlr4.Parser.prototype);
slapiParser.prototype.constructor = slapiParser;

Object.defineProperty(slapiParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

slapiParser.EOF = antlr4.Token.EOF;
slapiParser.T__0 = 1;
slapiParser.T__1 = 2;
slapiParser.T__2 = 3;
slapiParser.T__3 = 4;
slapiParser.T__4 = 5;
slapiParser.HTTPVERB = 6;
slapiParser.PATH = 7;
slapiParser.NUMBER = 8;
slapiParser.JSONOBJECT = 9;
slapiParser.ID = 10;
slapiParser.WS = 11;

slapiParser.RULE_file = 0;
slapiParser.RULE_constant = 1;
slapiParser.RULE_endpoint = 2;

function FileContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = slapiParser.RULE_file;
    return this;
}

FileContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FileContext.prototype.constructor = FileContext;

FileContext.prototype.constant = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ConstantContext);
    } else {
        return this.getTypedRuleContext(ConstantContext,i);
    }
};

FileContext.prototype.endpoint = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(EndpointContext);
    } else {
        return this.getTypedRuleContext(EndpointContext,i);
    }
};

FileContext.prototype.enterRule = function(listener) {
    if(listener instanceof slapiListener ) {
        listener.enterFile(this);
	}
};

FileContext.prototype.exitRule = function(listener) {
    if(listener instanceof slapiListener ) {
        listener.exitFile(this);
	}
};




slapiParser.FileContext = FileContext;

slapiParser.prototype.file = function() {

    var localctx = new FileContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, slapiParser.RULE_file);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 8; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 8;
            switch(this._input.LA(1)) {
            case slapiParser.T__0:
                this.state = 6;
                this.constant();
                break;
            case slapiParser.T__2:
                this.state = 7;
                this.endpoint();
                break;
            default:
                throw new antlr4.error.NoViableAltException(this);
            }
            this.state = 10; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===slapiParser.T__0 || _la===slapiParser.T__2);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ConstantContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = slapiParser.RULE_constant;
    return this;
}

ConstantContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ConstantContext.prototype.constructor = ConstantContext;

ConstantContext.prototype.ID = function() {
    return this.getToken(slapiParser.ID, 0);
};

ConstantContext.prototype.NUMBER = function() {
    return this.getToken(slapiParser.NUMBER, 0);
};

ConstantContext.prototype.JSONOBJECT = function() {
    return this.getToken(slapiParser.JSONOBJECT, 0);
};

ConstantContext.prototype.enterRule = function(listener) {
    if(listener instanceof slapiListener ) {
        listener.enterConstant(this);
	}
};

ConstantContext.prototype.exitRule = function(listener) {
    if(listener instanceof slapiListener ) {
        listener.exitConstant(this);
	}
};




slapiParser.ConstantContext = ConstantContext;

slapiParser.prototype.constant = function() {

    var localctx = new ConstantContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, slapiParser.RULE_constant);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 12;
        this.match(slapiParser.T__0);
        this.state = 13;
        this.match(slapiParser.ID);
        this.state = 14;
        this.match(slapiParser.T__1);
        this.state = 15;
        _la = this._input.LA(1);
        if(!(_la===slapiParser.NUMBER || _la===slapiParser.JSONOBJECT)) {
        this._errHandler.recoverInline(this);
        }
        else {
            this.consume();
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function EndpointContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = slapiParser.RULE_endpoint;
    return this;
}

EndpointContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
EndpointContext.prototype.constructor = EndpointContext;

EndpointContext.prototype.HTTPVERB = function() {
    return this.getToken(slapiParser.HTTPVERB, 0);
};

EndpointContext.prototype.PATH = function() {
    return this.getToken(slapiParser.PATH, 0);
};

EndpointContext.prototype.ID = function() {
    return this.getToken(slapiParser.ID, 0);
};

EndpointContext.prototype.enterRule = function(listener) {
    if(listener instanceof slapiListener ) {
        listener.enterEndpoint(this);
	}
};

EndpointContext.prototype.exitRule = function(listener) {
    if(listener instanceof slapiListener ) {
        listener.exitEndpoint(this);
	}
};




slapiParser.EndpointContext = EndpointContext;

slapiParser.prototype.endpoint = function() {

    var localctx = new EndpointContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, slapiParser.RULE_endpoint);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 17;
        this.match(slapiParser.T__2);
        this.state = 18;
        this.match(slapiParser.HTTPVERB);
        this.state = 19;
        this.match(slapiParser.T__3);
        this.state = 20;
        this.match(slapiParser.PATH);
        this.state = 21;
        this.match(slapiParser.T__4);
        this.state = 22;
        this.match(slapiParser.ID);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


exports.slapiParser = slapiParser;
