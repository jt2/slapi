var slapiParser = require('./slapiParser.js');
var slapiLexer = require('./slapiLexer.js');
var antlr = require('antlr4/index');

function build_ast(program){
	var mockFile = new antlr.InputStream(program); 
	var lexer = new slapiLexer.slapiLexer(mockFile);
	var tokens  = new antlr.CommonTokenStream(lexer);
	var parser = new slapiParser.slapiParser(tokens);

	var the_ast = {inputs : {}, endpoints : []};

	var tree = parser.file();
	var rules = parser.ruleNames

	function get_text_from_token(token){
		var start = token.symbol.start;
		var stop = token.symbol.stop + 1;
		return mockFile.strdata.slice(start, stop);
	}

	function walkTree(tree){
		for (var child of tree.children){
			if (rules[child.ruleIndex] === 'constant'){
				console.log('constant');
				console.log(get_text_from_token(child.children[1]));
				var name = get_text_from_token(child.children[1]);
				var value = get_text_from_token(child.children[3]);
				
				the_ast.inputs[name] = {'value' : value, 'type': 'constant'}


			}
			if (rules[child.ruleIndex] === 'endpoint'){
				var new_endpoint = {
					httpVerb: get_text_from_token(child.children[1]),
					path: get_text_from_token(child.children[3]),
					returnValue: get_text_from_token(child.children[5])
				};
				the_ast.endpoints.push(new_endpoint);
			}
		}
	}

	walkTree(tree);

	return the_ast;
}

exports.build_ast = build_ast;