grammar slapi;

file : (constant | endpoint)+ ;

constant : 'let' ID '=' (NUMBER | JSONOBJECT); 

endpoint : 'when' HTTPVERB 'at' PATH 'return' ID ;

HTTPVERB : ('get' | 'put') ;

PATH : ('/'[a-z]+)+ ;

NUMBER : [0-9]+ ;

JSONOBJECT : '{' (. | JSONOBJECT)+  '}' ;

ID : [a-z]+ ;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines