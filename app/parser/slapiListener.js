// Generated from slapi.g4 by ANTLR 4.5.2
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by slapiParser.
function slapiListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

slapiListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
slapiListener.prototype.constructor = slapiListener;

// Enter a parse tree produced by slapiParser#file.
slapiListener.prototype.enterFile = function(ctx) {
};

// Exit a parse tree produced by slapiParser#file.
slapiListener.prototype.exitFile = function(ctx) {
};


// Enter a parse tree produced by slapiParser#constant.
slapiListener.prototype.enterConstant = function(ctx) {
};

// Exit a parse tree produced by slapiParser#constant.
slapiListener.prototype.exitConstant = function(ctx) {
};


// Enter a parse tree produced by slapiParser#endpoint.
slapiListener.prototype.enterEndpoint = function(ctx) {
};

// Exit a parse tree produced by slapiParser#endpoint.
slapiListener.prototype.exitEndpoint = function(ctx) {
};



exports.slapiListener = slapiListener;